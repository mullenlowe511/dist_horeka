
var data = {};

var sendForm = function () {
	axios.post('http://api.feriahorecamakro.com.pe/api/users/', data)
	.then(function (response) {
		document.getElementById("process-one").style.display = "none";
		document.getElementById("process-three").style.display = "block";
		document.getElementById("errorgeneral").style.display = "none";
		document.body.scrollTop = document.documentElement.scrollTop = 0;
		fbq('track', 'CompleteRegistration');
	})
	
	.catch(function (error) {
		console.log(error.response.data)
		
		var validator = $( "#afiliate__form__validate-one" ).validate();

		document.getElementById("errorgeneral").style.display = "block";

		document.getElementById("process-one").style.display = "block";
		//document.getElementById("process-one").style.display = "none";

		var errorsArr = {};


		if (error.response && error.response.data && error.response.data.document_code) {
		   errorsArr.document_code = "El documento ingresado ya esta registrado."
		}

		if (error.response && error.response.data && error.response.data.email) {
		   errorsArr.email = "El email ingresado ya esta registrado."
		   
		}

		if (error.response && error.response.data && error.response.data.makro_passport) {
			errorsArr.makro_passport = "El pasaporte Makro ingresado ya esta registrado."

		}
		console.log(errorsArr)
		validator.showErrors(errorsArr)


		
	  //
	});
};


function postData(id) {

	var form = document.getElementById(id);
	var formData = new FormData(form);

	formData.forEach(function(value, key) {
		data[key] = value;
	});

	sendForm();

	/*if(id=="afiliate__form__validate-one") {
		document.getElementById("process-one").style.display = "none";
		document.getElementById("process-two").style.display = "block";
	}

	if(id=="afiliate__form__validate-two") {
		sendForm();
	}*/
}
